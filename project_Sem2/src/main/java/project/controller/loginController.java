package project.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.mindrot.jbcrypt.BCrypt;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.google.gson.Gson;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.GenericType;
import com.sun.jersey.api.client.WebResource;

import project.entity.users;

@Controller
public class loginController {
		@RequestMapping("/login")
		public String Login(Model model) {
			users a = new users();
			model.addAttribute("a",a);
			return "customer/login";
		}
		@RequestMapping("/logout")
		public String logout(Model model,HttpServletRequest request) {
			users a = new users();
			model.addAttribute("a",a);
			HttpSession session = request.getSession();
			Boolean role = Boolean.parseBoolean(String.valueOf(session.getAttribute("role")));
			session.removeAttribute("user");
			if(role) {
				return "redirect:/login";
			}else {
				return "redirect:/home";
			}
			
			
		}
		
		@RequestMapping("/postLogin")
		public String postLogin(Model model , @RequestParam("email")String email,HttpServletRequest request , @RequestParam("password")String password) {
			Client client = Client.create();
			WebResource webResources = client.resource("http://localhost:8083/api/getUserByEmail/"+email);
			String data = webResources.get(String.class);
			Gson son = new Gson();
			GenericType<users> listType = new GenericType<users>() {};
			users acc =son.fromJson(data,listType.getType());
			Boolean role = false;
			String pass = "";
			String mess = "";
			users accountUser = new users();
			if(acc==null) {
				mess="Email is not correct";
				model.addAttribute("mess",mess);
				return "customer/login";
			}
			
			pass = acc.getPassword();
			accountUser.setId(acc.getId());
			accountUser.setName(acc.getName());
			accountUser.setEmail(acc.getEmail());
			accountUser.setPhone(acc.getPhone());
			accountUser.setPassword(acc.getPassword());
			role = acc.getRole();
			if (BCrypt.checkpw(password, pass)) {
				HttpSession session = request.getSession();
				session.setAttribute("user",accountUser);
				session.setAttribute("user_id",accountUser.getId());
				session.setAttribute("user_name",accountUser.getName());
				session.setAttribute("user_phone",accountUser.getPhone());
				session.setAttribute("user_email",accountUser.getEmail());
				session.setAttribute("user_address",accountUser.getAddress());
				session.setAttribute("role",role);
				session.setAttribute("login",true);
				if(role==true) {
					
					return "redirect:/admin/";
				}else {
					return "redirect:/home";
				}
				
			}else {
				mess="Password is not correct";
				model.addAttribute("mess",mess);
				users a = new users();
				model.addAttribute("a",a);
				return "customer/login";
			}
		}
		
		@RequestMapping(value = {"/insertUser"})
		public String insertAccount(@Valid @ModelAttribute("a")users a,@RequestParam("re_password")String re_password ,Model model,HttpServletRequest request) {
			Client client = Client.create();
			WebResource webResources = client.resource("http://localhost:8083/api/getUserByEmail/"+a.getEmail());
			String data = webResources.get(String.class);
			Gson son = new Gson();
			GenericType<users> listType = new GenericType<users>() {};
			users acc =son.fromJson(data,listType.getType());
			if(acc==null) {
			String mess ="";
			if(a.getPassword().equals(re_password)) {
			
				a.setRole(false);

				BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
				String hashedPassword = passwordEncoder.encode(re_password);
				a.setPassword(hashedPassword);
				String data1 =	son.toJson(a);
				Client client1 = Client.create();
				WebResource webResources1 = client1.resource("http://localhost:8083/api/insertUser");
				ClientResponse clientResponse1 = webResources1.type("application/json").post(ClientResponse.class,data1);
				String res = clientResponse1.getEntity(String.class);
				users u = son.fromJson(res, users.class);
				
				if(u!=null) {
					mess="Register success!";
					model.addAttribute("messs",mess);
					return "redirect:/login";
				}else {
					mess="Register Fail!!";
					model.addAttribute("messs",mess);
					model.addAttribute("error","Insert Failed!");
					model.addAttribute("a",a);
					return "customer/login";
				}
			
			}else {
				mess="Confirm password is wrong!";
				model.addAttribute("messs",mess);
				return "customer/login";
			}
			
		}else {
			String mess="Email already exists";
			model.addAttribute("messs",mess);
			return "customer/login";
			}
		}
			

}
