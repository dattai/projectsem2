package project.controller;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.StandardOpenOption;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.google.gson.Gson;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.GenericType;
import com.sun.jersey.api.client.WebResource;

import project.dto.productDTO;
import project.entity.category;
import project.entity.product;

@Controller
@RequestMapping("/admin")
public class productController {
	Gson son = new Gson();
	Client client = Client.create();

	@RequestMapping("/listProduct")
	public String listProduct(Model model) {

		WebResource webResources = client.resource("http://localhost:8083/api/getListProduct");
		String data = webResources.get(String.class);

		GenericType<List<product>> listType = new GenericType<List<product>>() {
		};
		List<product> list = son.fromJson(data, listType.getType());
		model.addAttribute("list", list);
		return "admin/product/listProduct";
	}

	@RequestMapping("/insertProduct")
	public String insertProduct(Model model) {

		WebResource webResources = client.resource("http://localhost:8083/api/getListCategory");
		String data = webResources.get(String.class);
		GenericType<List<category>> listType = new GenericType<List<category>>() {
		};

		List<category> list = son.fromJson(data, listType.getType());
		model.addAttribute("list", list);
		product a = new product();
		model.addAttribute("a", a);
		return "admin/product/insertProduct";
	}

	@RequestMapping("/postInsertProduct")
	public String postInsertProduct(@Valid @ModelAttribute("a") productDTO a, BindingResult result, Model model,
			HttpServletRequest request) {
		String path = request.getServletContext().getRealPath("Resources/image");
		File f = new File(path);
		MultipartFile fileUpload = a.getFileUpload();
		File dest = new File(f.getAbsolutePath() + "/" + fileUpload.getOriginalFilename());
		if (!dest.exists()) {
			try {
				byte[] datImage = fileUpload.getBytes();
				Files.write(dest.toPath(), datImage, StandardOpenOption.CREATE);

			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		a.setImage(fileUpload.getOriginalFilename());

		category c = new category();
		c.setId(a.getCat_id());
		product p = new product(a.getId(), a.getName(), a.getPrice(), a.getSale_price(), a.getImage(),
				a.getDescription(), a.getStatus(), a.getQuantity(), c);
		String data = son.toJson(p);
		WebResource webResources = client.resource("http://localhost:8083/api/insertProduct");
		ClientResponse clientResponse = webResources.type("application/json").post(ClientResponse.class, data);
		String res = clientResponse.getEntity(String.class);
		product pro = son.fromJson(res, product.class);

		if (pro != null) {
			return "redirect:/admin/listProduct";
		} else {
			model.addAttribute("error", "Insert Failed!");
			model.addAttribute("a", a);
			return "admin/product/insertProduct";
		}
	}

	@RequestMapping("/deleteProduct")
	public String deleteProduct(@RequestParam("id") Integer id, Model model) {

		WebResource webResource = client.resource("http://localhost:8083/api/deleteProductById/" + id);
		ClientResponse clientResponse = webResource.type("application/json").delete(ClientResponse.class);
		String data = clientResponse.getEntity(String.class);

		if (data.equals("delete oke")) {
			model.addAttribute("success", "Delete Success");
		} else {
			model.addAttribute("error", "Delete Fail!");
		}
		return "redirect:/admin/listProduct";
	}

	@RequestMapping("/detailProduct")
	public String detailProduct(@RequestParam("id") Integer id, Model model) {

		WebResource webResources = client.resource("http://localhost:8083/api/getProductById/" + id);
		String data = webResources.get(String.class);
		GenericType<product> pro = new GenericType<product>() {
		};
		product p = son.fromJson(data, pro.getType());

		model.addAttribute("p", p);
		return "admin/product/detailProduct";
	}

	@RequestMapping("/updateProduct")
	public String updateProduct(@RequestParam("id") Integer id, Model model) {
		WebResource webResources1 = client.resource("http://localhost:8083/api/getListCategory");
		String data1 = webResources1.get(String.class);
		GenericType<List<category>> cat = new GenericType<List<category>>() {
		};

		List<category> c = son.fromJson(data1, cat.getType());
		model.addAttribute("c", c);

		WebResource webResources = client.resource("http://localhost:8083/api/getProductById/" + id);
		String data = webResources.get(String.class);
		GenericType<product> pro = new GenericType<product>() {
		};
		product p = son.fromJson(data, pro.getType());

		model.addAttribute("p", p);

		return "admin/product/updateProduct";
	}

	@RequestMapping("/postUpdateProduct")
	public String postUpdateProduct(@Valid @ModelAttribute("a") productDTO a, Model model, BindingResult result,HttpServletRequest request) {
		String path = request.getServletContext().getRealPath("Resources/image");
		File f = new File(path);
		MultipartFile fileUpload = a.getFileUpload();
		File dest = new File(f.getAbsolutePath() + "/" + fileUpload.getOriginalFilename());
		if (!dest.exists()) {
			try {
				byte[] datImage = fileUpload.getBytes();
				Files.write(dest.toPath(), datImage, StandardOpenOption.CREATE);

			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		a.setImage(fileUpload.getOriginalFilename());

		category c = new category();
		c.setId(a.getCat_id());
		product p = new product(a.getId(), a.getName(), a.getPrice(), a.getSale_price(), a.getImage(),
				a.getDescription(), a.getStatus(), a.getQuantity(), c);
		String data = son.toJson(p);
		WebResource webResources = client.resource("http://localhost:8083/api/insertProduct");
		ClientResponse clientResponse = webResources.type("application/json").post(ClientResponse.class, data);
		String res = clientResponse.getEntity(String.class);
		product pro = son.fromJson(res, product.class);
		if (pro != null) {
			return "redirect:/admin/listProduct";
		} else {
			model.addAttribute("error", "Update Failed!");
			model.addAttribute("a", a);
			return "admin/product/updateProduct";
		}
	}

}
