<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Karma Shop</title>
<!-- Mobile Specific Meta -->
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<!-- Favicon-->
<link rel="shortcut icon"
	href="<c:url value="Resources/customer"/>/img/fav.png">
<!-- Author Meta -->
<meta name="author" content="CodePixar">
<!-- Meta Description -->
<meta name="description" content="">
<!-- Meta Keyword -->
<meta name="keywords" content="">
<!-- meta character set -->
<meta charset="UTF-8">
<!-- Site Title -->
<title>Karma Shop</title>

<!--
		CSS
		============================================= -->
<link rel="stylesheet"
	href="<c:url value="Resources/customer"/>/css/linearicons.css">
<link rel="stylesheet"
	href="<c:url value="Resources/customer"/>/css/owl.carousel.css">
<link rel="stylesheet"
	href="<c:url value="Resources/customer"/>/css/themify-icons.css">
<link rel="stylesheet"
	href="<c:url value="Resources/customer"/>/css/font-awesome.min.css">
<link rel="stylesheet"
	href="<c:url value="Resources/customer"/>/css/nice-select.css">
<link rel="stylesheet"
	href="<c:url value="Resources/customer"/>/css/nouislider.min.css">
<link rel="stylesheet"
	href="<c:url value="Resources/customer"/>/css/bootstrap.css">
<link rel="stylesheet"
	href="<c:url value="Resources/customer"/>/css/main.css">
</head>

<body>

	<!-- Start Header Area -->
	<header class="header_area sticky-header">
		<div class="main_menu">
			<nav class="navbar navbar-expand-lg navbar-light main_box">
				<div class="container">
					<!-- Brand and toggle get grouped for better mobile display -->
					<a class="navbar-brand logo_h"
						href="${pageContext.request.contextPath}/"><img
						src="<c:url value="Resources/customer"/>/img/logo.png" alt=""></a>
					<button class="navbar-toggler" type="button" data-toggle="collapse"
						data-target="#navbarSupportedContent"
						aria-controls="navbarSupportedContent" aria-expanded="false"
						aria-label="Toggle navigation">
						<span class="icon-bar"></span> <span class="icon-bar"></span> <span
							class="icon-bar"></span>
					</button>
					<!-- Collect the nav links, forms, and other content for toggling -->
					<jsp:include page="nav.jsp"></jsp:include>
				</div>
			</nav>
		</div>
		
	</header>