
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<nav class="layout-footer footer bg-white">
	<div
		class="container-fluid d-flex flex-wrap justify-content-between text-center container-p-x pb-3">
		<div class="pt-3">
			<span class="footer-text font-weight-semibold">&copy; <a
				href="../<c:url value="Resources/admin"/>/https://srthemesvilla.com"
				class="footer-link" target="_blank">Srthemesvilla</a></span>
		</div>
		<div>
			<a href="../<c:url value="Resources/admin"/>/javascript:"
				class="footer-link pt-3">About Us</a> <a
				href="../<c:url value="Resources/admin"/>/javascript:"
				class="footer-link pt-3 ml-4">Help</a> <a
				href="../<c:url value="Resources/admin"/>/javascript:"
				class="footer-link pt-3 ml-4">Contact</a> <a
				href="../<c:url value="Resources/admin"/>/javascript:"
				class="footer-link pt-3 ml-4">Terms &amp; Conditions</a>
		</div>
	</div>
</nav>
<!-- [ Layout footer ] End -->
</div>
<!-- [ Layout content ] Start -->
</div>
<!-- [ Layout container ] End -->
</div>
<!-- Overlay -->
<div class="layout-overlay layout-sidenav-toggle"></div>
</div>
<!-- [ Layout wrapper] End -->

<!-- Core scripts -->
<script src="../<c:url value="Resources/admin"/>/assets/js/pace.js"></script>
<script
	src="../<c:url value="Resources/admin"/>/assets/js/jquery-3.3.1.min.js"></script>
<script
	src="../<c:url value="Resources/admin"/>/assets/libs/popper/popper.js"></script>
<script src="../<c:url value="Resources/admin"/>/assets/js/bootstrap.js"></script>
<script src="../<c:url value="Resources/admin"/>/assets/js/sidenav.js"></script>
<script
	src="../<c:url value="Resources/admin"/>/assets/js/layout-helpers.js"></script>
<script
	src="../<c:url value="Resources/admin"/>/assets/js/material-ripple.js"></script>

<!-- Libs -->
<script
	src="../<c:url value="Resources/admin"/>/assets/libs/perfect-scrollbar/perfect-scrollbar.js"></script>
<script src="../<c:url value="Resources/admin"/>/assets/libs/eve/eve.js"></script>
<script
	src="../<c:url value="Resources/admin"/>/assets/libs/flot/flot.js"></script>
<script
	src="../<c:url value="Resources/admin"/>/assets/libs/flot/curvedLines.js"></script>
<script
	src="../<c:url value="Resources/admin"/>/assets/libs/chart-am4/core.js"></script>
<script
	src="../<c:url value="Resources/admin"/>/assets/libs/chart-am4/charts.js"></script>
<script
	src="../<c:url value="Resources/admin"/>/assets/libs/chart-am4/animated.js"></script>

<!-- Demo -->

</body>

</html>