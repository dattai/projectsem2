USE [master]
GO
/****** Object:  Database [DB_project_Sem_2]    Script Date: 18/09/2022 4:22:03 PM ******/
CREATE DATABASE [DB_project_Sem_2]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'DB_project_Sem_2', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.SQLEXPRESS\MSSQL\DATA\DB_project_Sem_2.mdf' , SIZE = 3264KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'DB_project_Sem_2_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.SQLEXPRESS\MSSQL\DATA\DB_project_Sem_2_log.ldf' , SIZE = 816KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [DB_project_Sem_2] SET COMPATIBILITY_LEVEL = 120
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [DB_project_Sem_2].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [DB_project_Sem_2] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [DB_project_Sem_2] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [DB_project_Sem_2] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [DB_project_Sem_2] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [DB_project_Sem_2] SET ARITHABORT OFF 
GO
ALTER DATABASE [DB_project_Sem_2] SET AUTO_CLOSE ON 
GO
ALTER DATABASE [DB_project_Sem_2] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [DB_project_Sem_2] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [DB_project_Sem_2] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [DB_project_Sem_2] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [DB_project_Sem_2] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [DB_project_Sem_2] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [DB_project_Sem_2] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [DB_project_Sem_2] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [DB_project_Sem_2] SET  ENABLE_BROKER 
GO
ALTER DATABASE [DB_project_Sem_2] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [DB_project_Sem_2] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [DB_project_Sem_2] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [DB_project_Sem_2] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [DB_project_Sem_2] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [DB_project_Sem_2] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [DB_project_Sem_2] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [DB_project_Sem_2] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [DB_project_Sem_2] SET  MULTI_USER 
GO
ALTER DATABASE [DB_project_Sem_2] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [DB_project_Sem_2] SET DB_CHAINING OFF 
GO
ALTER DATABASE [DB_project_Sem_2] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [DB_project_Sem_2] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [DB_project_Sem_2] SET DELAYED_DURABILITY = DISABLED 
GO
USE [DB_project_Sem_2]
GO
/****** Object:  Table [dbo].[category]    Script Date: 18/09/2022 4:22:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[category](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](50) NULL,
	[status] [bit] NULL CONSTRAINT [DF__category__status__5FB337D6]  DEFAULT ((1)),
	[create_at] [datetime] NULL CONSTRAINT [DF__category__create__2739D489]  DEFAULT (getdate()),
 CONSTRAINT [PK__category__3213E83F808C9A7B] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[order_detail]    Script Date: 18/09/2022 4:22:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[order_detail](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[product_id] [int] NOT NULL,
	[quantity] [int] NULL,
	[orders_id] [int] NOT NULL,
	[price] [float] NULL,
	[sale_price] [float] NULL,
 CONSTRAINT [PK__order_de__3213E83FDB46603B] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[orders]    Script Date: 18/09/2022 4:22:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[orders](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[user_id] [int] NULL,
	[total] [float] NULL,
	[address] [nvarchar](100) NULL,
	[payment_id] [int] NULL,
	[create_at] [datetime] NULL CONSTRAINT [DF__order__create_at__02FC7413]  DEFAULT (getdate()),
	[status] [int] NULL,
	[user_name] [nvarchar](100) NULL,
	[phone] [int] NULL,
	[email] [nvarchar](100) NULL,
 CONSTRAINT [PK__order__3213E83FC45D70CE] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[product]    Script Date: 18/09/2022 4:22:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[product](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](100) NULL,
	[cat_id] [int] NULL,
	[price] [float] NULL,
	[sale_price] [float] NULL,
	[image] [nvarchar](50) NULL,
	[description] [text] NULL,
	[status] [bit] NULL DEFAULT ((1)),
	[quantity] [int] NULL,
	[create_at] [datetime] NULL CONSTRAINT [DF_product_create_at]  DEFAULT (getdate()),
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[users]    Script Date: 18/09/2022 4:22:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[users](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](100) NOT NULL,
	[gender] [bit] NULL,
	[address] [nvarchar](100) NULL,
	[email] [nvarchar](50) NOT NULL,
	[age] [int] NULL,
	[phone] [int] NULL,
	[password] [varchar](100) NOT NULL,
	[role] [bit] NULL CONSTRAINT [DF__users__role__108B795B]  DEFAULT ((0)),
	[create_date] [datetime] NOT NULL CONSTRAINT [DF__users__create_da__117F9D94]  DEFAULT (getdate()),
 CONSTRAINT [PK__users__3213E83F2D32D1B7] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [UQ__users__AB6E616404A32DFB] UNIQUE NONCLUSTERED 
(
	[email] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[order_detail]  WITH CHECK ADD  CONSTRAINT [fk_orders_id] FOREIGN KEY([orders_id])
REFERENCES [dbo].[orders] ([id])
GO
ALTER TABLE [dbo].[order_detail] CHECK CONSTRAINT [fk_orders_id]
GO
ALTER TABLE [dbo].[order_detail]  WITH CHECK ADD  CONSTRAINT [fk_product_id] FOREIGN KEY([product_id])
REFERENCES [dbo].[product] ([id])
GO
ALTER TABLE [dbo].[order_detail] CHECK CONSTRAINT [fk_product_id]
GO
ALTER TABLE [dbo].[orders]  WITH CHECK ADD  CONSTRAINT [fk_users_id] FOREIGN KEY([user_id])
REFERENCES [dbo].[users] ([id])
GO
ALTER TABLE [dbo].[orders] CHECK CONSTRAINT [fk_users_id]
GO
ALTER TABLE [dbo].[product]  WITH CHECK ADD  CONSTRAINT [fk_category_id] FOREIGN KEY([cat_id])
REFERENCES [dbo].[category] ([id])
GO
ALTER TABLE [dbo].[product] CHECK CONSTRAINT [fk_category_id]
GO
USE [master]
GO
ALTER DATABASE [DB_project_Sem_2] SET  READ_WRITE 
GO
