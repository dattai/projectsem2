package project;

import java.util.Collections;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EnableJpaRepositories(basePackages = "project.*")
@ComponentScan(basePackages = "project.*")
@EntityScan(basePackages = "project.*")

public class ApiProjectSem2Application {
	public static void main(String[] args) {
		SpringApplication app = new SpringApplication(ApiProjectSem2Application.class);
		 app.setDefaultProperties(Collections
		          .singletonMap("server.port", "8083"));
		        app.run(args);
	} 
}
