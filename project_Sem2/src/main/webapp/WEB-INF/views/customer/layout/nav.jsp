<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<div class="collapse navbar-collapse offset" id="navbarSupportedContent">
	<ul class="nav navbar-nav menu_nav ml-auto">
		<li class="nav-item "><a class="nav-link"
			href="${pageContext.request.contextPath}/home">Home</a></li>
		<li class="nav-item "><a class="nav-link"
			href="${pageContext.request.contextPath}/category">Shop</a></li>
		<li class="nav-item "><a class="nav-link"
			href="${pageContext.request.contextPath}/blog">Blog</a></li>
		<li class="nav-item "><a class="nav-link"
			href="${pageContext.request.contextPath}/contact">Contact</a></li>
	</ul>
	<ul class="nav navbar-nav navbar-right">
		<li class="nav-item"><a class="nav-link"
			href="${pageContext.request.contextPath}/cart" class="cart">Cart</a></li>

		<c:if test="${empty user}">
			<li class="nav-item"><a class="nav-link"
				href="${pageContext.request.contextPath}/login" class="cart">Login</a></li>
		</c:if>
		<c:if test="${!empty user}">
			<li class="nav-item submenu dropdown"><a href="#"
			class="nav-link dropdown-toggle" >${user.name}</a>
				<ul class="dropdown-menu">
				<li class="nav-item"><a href="${pageContext.request.contextPath}/listOrder" class="nav-link">Order</a></li>
				<li class="nav-item"><a href="${pageContext.request.contextPath}/logout" class="nav-link">Log Out</a></li>
				</ul></li>
		</c:if>
	</ul>
</div>