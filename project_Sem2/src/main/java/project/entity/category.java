package project.entity;

import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import lombok.ToString;
 
@Entity

@NoArgsConstructor
@AllArgsConstructor
@Data
@ToString
public class category {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	@NotEmpty(message = "Name cannot be left blank")
	private String name;
	
	private Boolean status;

	
//	@OneToMany(mappedBy = "id")
//	@OneToMany(targetEntity = product.class ,cascade = CascadeType.ALL)
//	@JoinColumn(name = "cat_id",referencedColumnName = "id")
//	private List<product> listPro;
	
//	@OneToMany(mappedBy = "objPro")
//	private Set<product> listPro;
}
