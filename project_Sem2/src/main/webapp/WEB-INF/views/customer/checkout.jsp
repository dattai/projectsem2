<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<jsp:include page="layout/header.jsp"></jsp:include>
 <c:if test="${empty user}">
  	 <jsp:forward page="/login"></jsp:forward>
  	</c:if>
  	 <c:if test="${role == true}">
  	 <jsp:forward page="/login"></jsp:forward>
  	</c:if>
<section class="checkout_area section_gap">
        <div class="container">
            <div class="billing_details">
                <div class="row">
                    <div class="col-lg-8">
                        <h3>Billing Details</h3>
                        <form:form class="row contact_form" action="postOrder" method="post" modelAttribute="order" novalidate="novalidate">
                            <div class="col-md-6 form-group p_star">
                            <input type="hidden"  class="form-control" value="${sessionScope.user_id}"id="name" name="user_id" >
                            <form:input type="text" class="form-control" value="${sessionScope.user_name}" id="name" path="user_name" 
									placeholder="Recipient Name" onfocus="this.placeholder = ''"
									onblur="this.placeholder = 'Recipient Name'"></form:input>
                            </div>
 
                            <div class="col-md-6 form-group p_star">
                             <form:input type="number" class="form-control" value="${sessionScope.user_phone }" id="name" path="phone" 
									placeholder="Phone Number" onfocus="this.placeholder = ''"
									onblur="this.placeholder = 'Phone Number'"></form:input>
                            </div>
                            
                            <div class="col-md-6 form-group p_star">
                             <form:input type="email" class="form-control" value="${sessionScope.user_email }" id="name" path="email" 
									placeholder="Email" onfocus="this.placeholder = ''"
									onblur="this.placeholder = 'Email'"></form:input>
                            </div>
                            <div class="col-md-12 form-group">
                                
                                <form:textarea class="form-control" value="${sessionScope.user_address }" path="address" name="message" id="message" rows="1" placeholder="Address "></form:textarea>
                            </div>                        
                            <button type="submit" class="primary-btn" >Proceed to Paypal</button>
                        </form:form>
                    </div>
                    <div class="col-lg-4">
                        <div class="order_box">
                            <h2>Your Order</h2>
                            <ul class="list">
                                <li><a href="#">Product <span>Total</span></a></li>
                                <c:forEach var="map" items="${sessionScope.myCartItems}">
                                <li><a href="#">${map.value.product.name} <span class="middle">x ${map.value.quantity}</span> <span class="last">$ ${map.value.quantity * map.value.product.price}</span></a></li>
                                </c:forEach>
                            </ul>
                            <ul class="list list_2">
                                <li><a href="#">Subtotal <span>$ ${sessionScope.myCartTotal}</span></a></li>
                                <li><a href="#">Shipping <span>Flat rate: $1.00</span></a></li>
                                <li><a href="#">Total <span>$ ${sessionScope.myCartTotal +1 }</span></a></li>
                            </ul>
                         
                            </div>
                           
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
<jsp:include page="layout/footer.jsp"></jsp:include>