package project.dto;

import org.springframework.web.multipart.MultipartFile;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class productDTO {
	private Integer id ;
	
	private String name ;
	
	private float price ;
	
	private float sale_price; 
	
	private String image ;
	
	private String description; 
	
	private Boolean status ;
	
	private Integer quantity;
	
	private Integer cat_id; 
	
	private MultipartFile fileUpload;
}
