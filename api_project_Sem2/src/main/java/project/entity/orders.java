package project.entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Data
public class orders {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
	private String user_name;
	private Integer phone; 
	
	private float total;
	
	@ManyToOne
	@JoinColumn(name = "user_id")
	private users users;

	@OneToMany(targetEntity = order_detail.class ,cascade = CascadeType.ALL)
	@JoinColumn(name = "orders_id",referencedColumnName = "id")
	private List<order_detail> order_detail;
    
	private Integer status;
	
	private String address;
	private String email;
}
