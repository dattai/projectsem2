package project.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.google.gson.Gson;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.GenericType;
import com.sun.jersey.api.client.WebResource;

import project.entity.cart;
import project.entity.category;
import project.entity.order_detail;
import project.entity.orders;
import project.entity.product;
import project.entity.users;

@Controller
public class cartController {
	Gson son = new Gson();
	Client client = Client.create();

	@RequestMapping("/addCart")
	public String addCart(ModelMap modelMap, HttpSession session, @RequestParam("id") Integer id) {
		HashMap<Integer, cart> cartItems = (HashMap<Integer, cart>) session.getAttribute("myCartItems");

		if (cartItems == null) {
			cartItems = new HashMap<>();
		}

		WebResource webResources = client.resource("http://localhost:8083/api/getProductById/" + id);
		String data = webResources.get(String.class);
		GenericType<product> pro = new GenericType<product>() {
		};
		product product = son.fromJson(data, pro.getType());

		if (product != null) {
			if (cartItems.containsKey(id)) {
				cart item = cartItems.get(id);
				item.setProduct(product);
				item.setQuantity(item.getQuantity() + 1);
				cartItems.put(id, item);
			} else {
				cart item = new cart();
				item.setProduct(product);
				item.setQuantity(1);
				cartItems.put(id, item);
			}
		}
		session.setAttribute("myCartItems", cartItems);
		session.setAttribute("myCartTotal", totalPrice(cartItems));
		session.setAttribute("myCartNum", cartItems.size());
		return "redirect:/home";
	}

    @RequestMapping(value = "removeCart")
    public String viewRemove(ModelMap mm, HttpSession session, @RequestParam("id") Integer id) {
        HashMap<Integer, cart> cartItems = (HashMap<Integer, cart>) session.getAttribute("myCartItems");
        if (cartItems == null) {
            cartItems = new HashMap<>();
        }
        if (cartItems.containsKey(id)) {
            cartItems.remove(id);
        }
        session.setAttribute("myCartItems", cartItems);
        session.setAttribute("myCartTotal", totalPrice(cartItems));
        session.setAttribute("myCartNum", cartItems.size());
        return "customer/cart";
    }

    public Float totalPrice(HashMap<Integer, cart> cartItems) {
        float count = 0;
        for (Map.Entry<Integer, cart> list : cartItems.entrySet()) {
            count += list.getValue().getProduct().getPrice() * list.getValue().getQuantity();
        }
        return count;
    }
    @RequestMapping(value = "postOrder")
    public String postCheckout(ModelMap mm,Model model, HttpSession session, @ModelAttribute("order") orders orders,@RequestParam("user_id")String a) {
            HashMap<Integer, cart> cartItems = ( HashMap<Integer, cart>) session.getAttribute("myCartItems");
            if (cartItems == null) {
                cartItems = new HashMap<>();
            }
            users u = new users();
            
            u.setId(Integer.parseInt(a));
            orders.setStatus(0);
            orders.setUsers(u);
            orders.setTotal(totalPrice(cartItems)+1);
            String data = son.toJson(orders);
			WebResource webResources = client.resource("http://localhost:8083/api/insertOrder");
			ClientResponse clientResponse = webResources.type("application/json").post(ClientResponse.class, data);
			String res = clientResponse.getEntity(String.class);
			orders o = son.fromJson(res, orders.class);
            
            for (Map.Entry<Integer, cart> entry : cartItems.entrySet()) {
                order_detail order_detail = new order_detail();
                order_detail.setOrders_id(o.getId());
                order_detail.setProduct(entry.getValue().getProduct());
                order_detail.setPrice(entry.getValue().getProduct().getPrice());
                order_detail.setPrice(entry.getValue().getProduct().getSale_price());
                order_detail.setQuantity(entry.getValue().getQuantity());
                order_detail.setSale_price(1);
                
                String data1 = son.toJson(order_detail);
    			WebResource webResources1 = client.resource("http://localhost:8083/api/insertOrderDetail");
    			ClientResponse clientResponse1 = webResources1.type("application/json").post(ClientResponse.class, data1);
    			String res1 = clientResponse1.getEntity(String.class);
    			order_detail od = son.fromJson(res1, order_detail.class);
            }
            cartItems = new HashMap<>();
            session.setAttribute("myCartItems", cartItems);
            session.setAttribute("myCartTotal", 0);
            session.setAttribute("myCartNum", 0);
            
			
            model.addAttribute("o", o);
            
            return "customer/confirmation";
    }

}
