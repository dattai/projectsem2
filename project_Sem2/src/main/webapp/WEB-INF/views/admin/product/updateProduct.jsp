<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<jsp:include page="../layout/head.jsp"></jsp:include>
<div class="container-fluid flex-grow-1 container-p-y">
	<h4 class="font-weight-bold py-3 mb-0">Insert Product</h4>
	<div class="text-muted small mt-0 mb-4 d-block breadcrumb">
		<ol class="breadcrumb">
			<li class="breadcrumb-item"><a href="../admin/"><i
					class="feather icon-home"></i></a></li>
			<li class="breadcrumb-item">List Product</li>
			<li class="breadcrumb-item active">Insert Product</li>
		</ol>
	</div>

	<div class="card mb-4">
		<div class="card-body">
			<form:form action="postUpdateProduct" novalidate="novalidate"
				modelAttribute="p" method="post" enctype="multipart/form-data">
				<div class="form-group col-md-6">
					<label class="form-label">ID</label> <input type="text"
						class="form-control" name="id" readonly value="${p.id}">
					<div class="clearfix"></div>
				</div>
				<div class="form-row">

					<div class="form-group col-md-6">
						<label class="form-label">Name</label> <input type="text"
							class="form-control" name="name" value="${p.name }"
							placeholder="Name">
						<div class="clearfix"></div>
					</div>
					<div class="form-group col-md-6">
						<label class="form-label">Category Name</label> <select
							name="cat_id" class="custom-select">
							<c:forEach items="${c}" var="a">
								<option value="${a.id }">${a.name }</option>
							</c:forEach>
						</select>
					</div>
				</div>
				<div class="form-row">
					<div class="form-group col-md-6">
						<label class="form-label">Price</label> <input type="number"
							class="form-control" value="${p.price }" name="price"
							placeholder="Name">
						<div class="clearfix"></div>
					</div>
					<div class="form-group col-md-6">
						<label class="form-label">Sale Price</label> <input type="number"
							class="form-control" value="${p.sale_price }" name="sale_price"
							placeholder="Sale Price">
						<div class="clearfix"></div>
					</div>
				</div>
				<div class="form-row">

					<div class="form-group col-md-6">
						<label class="form-label">Quantity</label> <input type="number"
							class="form-control" value="${p.quantity }" name="quantity"
							placeholder="Quantity">
						<div class="clearfix"></div>
					</div>
					<div class="form-group col-md-6">
						<div class="row">
							<label class="col-form-label col-sm-3 text-sm-right pt-sm-0">Status</label>
							<div class="col-sm-9">
								<div class="custom-controls-stacked">
									<label class="custom-control custom-radio"> <input
										name="status" type="radio" class="custom-control-input"
										value="true" ${p.status?"checked":" " }> <span
										class="custom-control-label">Show</span>
									</label> <label class="custom-control custom-radio"> <input
										name="status" type="radio" class="custom-control-input"
										value="false" ${p.status?" ":"checked" }> <span
										class="custom-control-label">Hide</span>
									</label>
								</div>
							</div>
						</div>

					</div>
				</div>
				<div class="form-group row ">
					<div class="form-group col-md-9">
						<label class="form-label">Description </label>
						<textarea class="form-control" name="description"
							placeholder="Description" style="height: 123px;">${p.description}</textarea>
					</div>
					<div class="form-group col-md-3">
						<label class="form-label w-100">Choses Image</label> 
							<input  type="file" name="fileUpload"/>
							<input  type="hidden" value="../<c:url value="Resources/image"/>/${p.image}" name="fileUpload1"/>
					</div>
				</div>

				<button type="submit" class="btn btn-primary">Submit</button>
			</form:form>
		</div>
	</div>
</div>

<jsp:include page="../layout/footer.jsp"></jsp:include>
