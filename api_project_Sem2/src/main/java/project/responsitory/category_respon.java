package project.responsitory;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import project.entity.category;

@Repository
public interface category_respon extends JpaRepository<category,Integer> {
	category findByid(Integer id);
}
