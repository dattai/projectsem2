package project.controller;

import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.google.gson.Gson;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.GenericType;
import com.sun.jersey.api.client.WebResource;

import project.entity.category;
import project.entity.orders;
import project.entity.product;

@Controller
public class customerController {
	
	Gson son = new Gson();
	Client client = Client.create();

	@RequestMapping(value = { "/", "/home" })
	public String home(Model model) {
		WebResource webResources = client.resource("http://localhost:8083/api/getListProduct");
		String data = webResources.get(String.class);
		GenericType<List<product>> listType = new GenericType<List<product>>() {
		};
		List<product> list = son.fromJson(data, listType.getType());
		model.addAttribute("list", list);
		return "customer/home";
	}

	@RequestMapping(value = { "/category" })
	public String category(Model model) {
		WebResource webResources = client.resource("http://localhost:8083/api/getListProduct");
		String data = webResources.get(String.class);
		GenericType<List<product>> listType = new GenericType<List<product>>() {
		};
		List<product> list = son.fromJson(data, listType.getType());
		model.addAttribute("list", list);
		return "customer/category";
	}

	@RequestMapping(value = { "/productDetail" })
	public String productDetail(Model model, @RequestParam("id") String id) {
		
		WebResource webResources = client.resource("http://localhost:8083/api/getProductById/" + id);
		String data = webResources.get(String.class);
		GenericType<product> listType = new GenericType<product>() {};
		product c = son.fromJson(data, listType.getType());
		model.addAttribute("a", c);
		return "customer/productDetail";
	}

	@RequestMapping(value = { "/blog" })
	public String blog(Model model) {
		return "customer/blog";
	}

	@RequestMapping(value = { "/contact" })
	public String contact(Model model) {
		return "customer/contact";
	}
	@RequestMapping(value = { "/cart" })
	public String cart(Model model) {
		return "customer/cart";
	}
	@RequestMapping(value = { "/checkout" })
	public String checkout(Model model) {
		orders o = new orders();
		model.addAttribute("order", o);
		return "customer/checkout";
	}
}
