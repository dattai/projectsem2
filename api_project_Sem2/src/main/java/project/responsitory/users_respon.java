package project.responsitory;

import org.springframework.data.jpa.repository.JpaRepository;

import project.entity.users;
public interface users_respon extends JpaRepository<users, Integer>{
	users findByEmail(String email);
	void deleteById(Integer id);
}
