<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<jsp:include page="../layout/head.jsp"></jsp:include>

<div class="layout-content">

	<!-- [ content ] Start -->
	<div class="container-fluid flex-grow-1 container-p-y">
		<h4 class="font-weight-bold py-3 mb-0">Insert Category</h4>
		<div class="text-muted small mt-0 mb-4 d-block breadcrumb">
			<ol class="breadcrumb">
				<li class="breadcrumb-item"><a href="../admin/"><i
						class="feather icon-home"></i></a></li>
				<li class="breadcrumb-item">List Category</li>
				<li class="breadcrumb-item">Insert Category</li>
			</ol>
		</div>
		<div class="card mb-4">
			<div class="card-body">
				<form:form action="postInsertCategory"
							novalidate="novalidate" modelAttribute="a" method="post">
					<div class="form-group row">
						<label class="col-form-label col-sm-2 text-sm-right">Name</label>
						<div class="col-sm-10">
							<form:input type="text" class="form-control" path="name" placeholder="Name"></form:input>
							<form:errors  style="color:red" path="name"></form:errors>
							<div class="clearfix"></div>
						</div>
					</div>
					<fieldset class="form-group">
						<div class="row">
							<label class="col-form-label col-sm-2 text-sm-right pt-sm-0" >Status</label>
							<div class="col-sm-10">
								<div class="custom-controls-stacked">
									<label class="custom-control custom-radio"> <input
										name="status" type="radio"
										class="custom-control-input" value="true" checked> <span
										class="custom-control-label">Show</span>
									</label> <label class="custom-control custom-radio"> <input
										name="status" type="radio"
										class="custom-control-input" value="false"> <span
										class="custom-control-label">Hide</span>
									</label>
								</div>
							</div>
						</div>
					</fieldset>
					<div class="form-group row">
						<div class="col-sm-10 ml-sm-auto">
							<button type="submit" value="submit" class="btn btn-success">Submit</button>
							<a href="../admin/listCategory" type="reset" class="btn btn-danger">Back</a>
						</div>
					</div>
				</form:form>
			</div>
		</div>
	</div>
</div>
<jsp:include page="../layout/footer.jsp"></jsp:include>
