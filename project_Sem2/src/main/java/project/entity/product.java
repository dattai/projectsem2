package project.entity;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.springframework.web.multipart.MultipartFile;

import lombok.AllArgsConstructor;
import lombok.Data;

import lombok.NoArgsConstructor;


@Entity
@NoArgsConstructor
@AllArgsConstructor
@Data
public class product {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id ;
	
	@NotEmpty(message = "Name cannot be left blank")
	private String name ;
	
	@NotNull(message = "Price cannot be left blank")
	private float price ;
	
	private float sale_price; 
	
	private String image ;
	
	private String description; 
	
	private Boolean status ;
	@NotNull(message = "Quantity cannot be left blank")
	private Integer quantity; 
	
	
	
	@ManyToOne
	@JoinColumn(name = "cat_id")
	private category cat_id ;
//	@ManyToOne
//	@JoinColumn(name="cat_id",referencedColumnName = "id")
//	private category objPro;
}
