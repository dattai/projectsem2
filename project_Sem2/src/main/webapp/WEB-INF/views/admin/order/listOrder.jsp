<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<jsp:include page="../layout/head.jsp"></jsp:include>
 <div class="container-fluid flex-grow-1 container-p-y">
                   <h4 class="font-weight-bold py-3 mb-0">List Order
                   <a href="../admin/insertProduct" type="button" style="float: right" class="btn btn-sm btn-success"><span class="feather icon-plus-circle"></span> Insert</a>
                   </h4>
                   
                   <div class="text-muted small mt-0 mb-4 d-block breadcrumb">
                       <ol class="breadcrumb">
                           <li class="breadcrumb-item"><a href="../admin/"><i class="feather icon-home"></i></a></li>
                           <li class="breadcrumb-item">list ordert</li>
                       </ol>
                   </div>
       <table class="table">
                       <thead class="thead-light">
                           <tr>
                               <th>Id</th>
                               <th>User Name</th>
                               <th>Phone</th>
                               <th>address</th>
                               <th>total</th>
                               <th>Action</th>
                           </tr>
                       </thead>
                       <tbody>
                        <c:forEach  items="${list}" var="a">
                           <tr>
                               <td>${a.id }</td>
                               <td>${a.user_name }</td>
                               <td>${a.phone }</td>
                               <td>${a.address }</td>
                               <td>${a.total }</td>
                               <td><a href="../admin/detailProduct?id=${a.id}" type="button" class="btn btn-sm btn-info waves-effect"><span class="feather icon-folder"></span> detail</a>
                               <a href="../admin/deleteProduct?id=${a.id}" onclick="return confirm('Are you sure you want to Delete?');" type="button" class="btn btn-sm btn-danger"><span class="lnr lnr-cross"></span> Delete</a></td>
                              
                           </tr>
                            </c:forEach>
                       </tbody> 
                   </table>
                 </div>        
 <jsp:include page="../layout/footer.jsp"></jsp:include>
