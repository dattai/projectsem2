package project.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;


import lombok.AllArgsConstructor;
import lombok.Data;

import lombok.NoArgsConstructor;


@Entity
@NoArgsConstructor
@AllArgsConstructor
@Data
public class product {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id ;
	
	private String name ;
	
	private float price ;
	
	private float sale_price; 
	
	private String image ;
	
	private String description; 
	
	private Boolean status ;
	
	private Integer quantity; 

//	@ManyToOne
//	@JoinColumn(name = "cat_id")
//	private category catid ;
	@ManyToOne
	@JoinColumn(name="cat_id",referencedColumnName = "id")
	private category cat_id;
}
