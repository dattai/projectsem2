package project.responsitory;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import project.entity.order_detail;

@Repository
public interface orderDetail_respon extends JpaRepository<order_detail, Integer> {

}
