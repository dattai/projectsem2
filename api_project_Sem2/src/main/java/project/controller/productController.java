package project.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import project.entity.product;

import project.responsitory.product_respon;

@Controller
@RequestMapping("/api")
public class productController {
	@Autowired
	private product_respon product_respon;
	
	@ResponseBody
    @GetMapping("/getListProduct")
    public List<product> getlistproduct() {
    	return product_respon.findAll();
    }
    @ResponseBody
    @PostMapping("/insertProduct")
    public product insertproduct(@RequestBody product c ) {
    	return product_respon.save(c);
    }
    @ResponseBody
    @PutMapping("/updateProduct")
    public product updateproduct(@RequestBody product c) {
        return product_respon.save(c);
	}
    @ResponseBody
    @DeleteMapping("/deleteProductById/{id}")
    public String deleteproductById(@PathVariable Integer id) {
    	product_respon.deleteById(id);
    	return "delete oke" ;
    }
    @ResponseBody
    @GetMapping("/getProductById/{id}")
    public product getproductById(@PathVariable Integer id) {
    	return product_respon.findByid(id);
    }
}
