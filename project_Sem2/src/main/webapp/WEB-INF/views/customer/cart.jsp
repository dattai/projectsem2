<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<jsp:include page="layout/header.jsp"></jsp:include>
<section class="banner-area organic-breadcrumb">
	<div class="container">
		<div
			class="breadcrumb-banner d-flex flex-wrap align-items-center justify-content-end">
			<div class="col-first">
				<h1>Shopping Cart</h1>
				<nav class="d-flex align-items-center">
					<a href="index.html">Home<span class="lnr lnr-arrow-right"></span></a>
					<a href="category.html">Cart</a>
				</nav>
			</div>
		</div>
	</div>
</section>
<!-- End Banner Area -->

<!--================Cart Area =================-->
<section class="cart_area">
	<div class="container">
		<div class="cart_inner">
			<div class="table-responsive">
				<table class="table">
					<thead>
						<tr>
							<th scope="col">Product</th>
							<th scope="col">Price</th>
							<th scope="col">Quantity</th>
							<th scope="col">Total</th>
						</tr>
					</thead>
					<tbody>
						<c:forEach var="map" items="${sessionScope.myCartItems}">
							<tr>
								<td>
									<div class="media">
										<div class="d-flex">
											<img height="100" width="150"
												src="${pageContext.request.contextPath}/<c:url value="Resources/image"/>/${map.value.product.image}"
												alt="">
										</div>
										<div class="media-body">
											<p>
												<c:out value="${map.value.product.name}" />
											</p>
										</div>
									</div>
								</td>
								<td>
									<h5>$ ${map.value.product.price}</h5>
								</td>
								<td>
									<div class="product_count">
										<input type="text" value="${map.value.quantity}" title="Quantity:" class="input-text qty">
										<a href="${pageContext.request.contextPath}/removeCart?id=${map.value.product.id}" class="increase items-count" type="button"><span class="ti-close"></span></a>
									
									</div>
								</td>
								<td>
									<h5>$${map.value.quantity * map.value.product.price}</h5>
								</td>
							</tr>
						</c:forEach>


						<tr class="bottom_button">
							<td><a class="gray_btn" href="#">Update Cart</a></td>
							<td></td>
							<td>
								<h5>Subtotal</h5>
							</td>
							<td>
								<h5>
									$<c:out value="${sessionScope.myCartTotal}" />
								</h5>
							</td>
						</tr>

						<tr class="out_button_area">
							<td></td>
							<td></td>
							<td></td>
							<td>
								<div class="checkout_btn_inner d-flex align-items-center">
									<a class="gray_btn" href="${pageContext.request.contextPath}/">Home</a> <a
										class="primary-btn" href="${pageContext.request.contextPath}/checkout">Proceed to checkout</a>
								</div>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</section>
<jsp:include page="layout/footer.jsp"></jsp:include>