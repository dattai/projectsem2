<%@ page language="java" contentType="text/html; charset=UTF-8" 
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<div id="layout-sidenav" class="layout-sidenav sidenav sidenav-vertical bg-dark">
                <!-- Brand demo (see assets/css/demo/demo.css) -->
                <div class="app-brand demo">
                    <span class="app-brand-logo demo">
                        <img src="../<c:url value="Resources/admin"/>/assets/img/logo.png" alt="Brand Logo" class="img-fluid">
                    </span>
                    <a href="../admin/" class="app-brand-text demo sidenav-text font-weight-normal ml-2">Bhumlu</a>

                </div>
                <div class="sidenav-divider mt-0"></div>

                <!-- Links -->
                <ul class="sidenav-inner py-1">

                    <!-- Dashboards -->
                    <li class="sidenav-item active">
                        <a href="../admin/" class="sidenav-link">
                            <i class="sidenav-icon feather icon-home"></i>
                            <div>Dashboards</div>
                            <div class="pl-1 ml-auto">
                                <div class="badge badge-danger">Hot</div>
                            </div>
                        </a>
                    </li>


                   
                    <!-- Forms & Tables -->
                    <li class="sidenav-divider mb-1"></li>
                    
                    <li class="sidenav-header small font-weight-semibold">Forms & Tables</li>
                    <li class="sidenav-item">
                        <a href="../admin/listCategory" class="sidenav-link ">
                            <i class="sidenav-icon feather icon-clipboard"></i>
                            <div>Category</div>
                        </a>
                    </li>
                    <li class="sidenav-item">
                        <a href="../admin/listProduct" class="sidenav-link ">
                            <i class="sidenav-icon feather icon-box"></i>
                            <div>Product</div>
                        </a>
                    </li>
                     <li class="sidenav-item">
                        <a href="../admin/listOrder" class="sidenav-link ">
                            <i class="sidenav-icon feather icon-bar-chart-2"></i>
                            <div>Order</div>
                        </a>
                    </li>
                    <!-- Pages -->
                    <li class="sidenav-divider mb-1"></li>
                    <li class="sidenav-header small font-weight-semibold">Pages</li>
                    <li class="sidenav-item">
                        <a href="${pageContext.request.contextPath}/logout" class="sidenav-link">
                            <i class="sidenav-icon feather icon-lock"></i>
                            <div>Log Out</div>
                        </a>
                    </li>

                </ul>
            </div>