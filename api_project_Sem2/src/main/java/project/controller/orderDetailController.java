package project.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import project.entity.order_detail;
import project.entity.orders;
import project.responsitory.orderDetail_respon;

@Controller
@RequestMapping("/api")
public class orderDetailController {
	@Autowired
	private orderDetail_respon orderDetail_respon;
	
	@ResponseBody
    @GetMapping("/getListOrderDetail")
    public List<order_detail> getListOrderDetail() {
    	
    	return orderDetail_respon.findAll();
    }
    
    @ResponseBody
    @PostMapping("/insertOrderDetail")
    public order_detail insertproduct(@RequestBody order_detail o ) {
    	return orderDetail_respon.save(o);
    }
    
}
