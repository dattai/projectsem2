package project.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;

import com.google.gson.Gson;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.GenericType;
import com.sun.jersey.api.client.WebResource;

import project.entity.category;

@Controller
@RequestMapping("/admin")
public class categoryController {
	Gson son = new Gson();
	Client client = Client.create();
	@RequestMapping("/listCategory")
	public String listCategory(Model model) {
		
		WebResource webResources = client.resource("http://localhost:8083/api/getListCategory");
		String data = webResources.get(String.class);

		GenericType<List<category>> listType = new GenericType<List<category>>() {};
		List<category> list = son.fromJson(data, listType.getType());
		model.addAttribute("list", list);
		return "admin/category/listCategory";
	}

	@RequestMapping("/insertCategory")
	public String insertCategory(Model model) {
		category a = new category();
		model.addAttribute("a", a);
		return "admin/category/insertCategory";
	}

	@RequestMapping("/postInsertCategory")
	public String insertCategory(@Valid @ModelAttribute("a") category a, BindingResult result, Model model) {
		if (result.hasErrors()) {
			model.addAttribute("a", a);
			return "admin/category/insertCategory";
		} else {

			String data = son.toJson(a);
			WebResource webResources = client.resource("http://localhost:8083/api/insertCategory");
			ClientResponse clientResponse = webResources.type("application/json").post(ClientResponse.class, data);
			String res = clientResponse.getEntity(String.class);
			category c = son.fromJson(res, category.class);

			if (c!=null) {
				
				return "redirect:/admin/listCategory";
			} else {
				model.addAttribute("error", "Insert Failed!");
				model.addAttribute("a", a);
				return "admin/category/insertCategory";
			}
		}
	}
	@RequestMapping("/updateCategory")
	public String updateCategory(Model model,@RequestParam("id")String id) {

		WebResource webResources = client.resource("http://localhost:8083/api/getCategoryById/"+id);
		String data = webResources.get(String.class);

		GenericType<category> listType = new GenericType<category>() {
		};
		category c = son.fromJson(data, listType.getType());
	
		model.addAttribute("a", c);
		return "admin/category/updateCategory";
	}
	@RequestMapping("/postUpdateCategory")
	public String postUpdateCategory(@Valid @ModelAttribute("a") category a, Model model,BindingResult result) {
		if (result.hasErrors()) {
			model.addAttribute("a", a);
			return "admin/category/updateCategory";
		} else {

			String data = son.toJson(a);

			WebResource webResources = client.resource("http://localhost:8083/api/updateCategory");
			ClientResponse clientResponse = webResources.type("application/json").put(ClientResponse.class, data);
			String res = clientResponse.getEntity(String.class);
			category c = son.fromJson(res, category.class);

			if (c!=null) {
				
				return "redirect:/admin/listCategory";
			} else {
				model.addAttribute("error", "Update Failed!");
				model.addAttribute("a", a);
				return "admin/category/updateCategory";
			}
		}
	}

	@RequestMapping("/deleteCategory")
	public String deleteCategory(@RequestParam("id")Integer id, Model model) {
		Client client = Client.create();

		WebResource webResource = client.resource("http://localhost:8083/api/deleteCategoryById/"+id);
		ClientResponse clientResponse = webResource.type("application/json").delete(ClientResponse.class);
		String data = clientResponse.getEntity(String.class);
		
		
		if(data.equals("delete oke")) {
			model.addAttribute("success","Delete Success");
		}else {
			model.addAttribute("error","Delete Fail!");
		}
		return "redirect:/admin/listCategory";
	}
}
