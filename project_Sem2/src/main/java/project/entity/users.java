package project.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotEmpty;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Data
public class users{
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	@NotEmpty(message = "Name cannot be left blank")
	private String name;
	
	private Boolean gender;

	private String address;
	
	@NotEmpty(message = "Email cannot be left blank")
	private String email;

	private String age;
	
	private Integer phone;
	
	@NotEmpty(message = "Password cannot be left blank ")
	private String password;

	private Boolean role;
	
	
}
