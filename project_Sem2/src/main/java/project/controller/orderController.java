package project.controller;

import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.google.gson.Gson;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.GenericType;
import com.sun.jersey.api.client.WebResource;

import project.entity.orders;


@Controller
@RequestMapping("/admin")
public class orderController {
	Gson son = new Gson();
	Client client = Client.create();

	@RequestMapping("/listOrder")
	public String listOrder(Model model) {

		WebResource webResources = client.resource("http://localhost:8083/api/getListOrder");
		String data = webResources.get(String.class);

		GenericType<List<orders>> listType = new GenericType<List<orders>>() {
		};
		List<orders> list = son.fromJson(data, listType.getType());
		model.addAttribute("list", list);
		return "admin/order/listOrder";
	}
}
