<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Karma Shop</title>
<!-- Mobile Specific Meta -->
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<!-- Favicon-->
<link rel="shortcut icon"
	href="${pageContext.request.contextPath}/<c:url value="Resources/customer"/>/img/fav.png">
<!-- Author Meta -->
<meta name="author" content="CodePixar">
<!-- Meta Description -->
<meta name="description" content="">
<!-- Meta Keyword -->
<meta name="keywords" content="">
<!-- meta character set -->
<meta charset="UTF-8">
<!-- Site Title -->
<title>Karma Shop</title>

<!--
		CSS
		============================================= -->
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/<c:url value="Resources/customer"/>/css/linearicons.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/<c:url value="Resources/customer"/>/css/owl.carousel.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/<c:url value="Resources/customer"/>/css/themify-icons.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/<c:url value="Resources/customer"/>/css/font-awesome.min.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/<c:url value="Resources/customer"/>/css/nice-select.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/<c:url value="Resources/customer"/>/css/nouislider.min.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/<c:url value="Resources/customer"/>/css/bootstrap.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/<c:url value="Resources/customer"/>/css/main.css">
</head>

<body>

	<!-- Start Header Area -->
	<header class="header_area sticky-header">
		<div class="main_menu">
			<nav class="navbar navbar-expand-lg navbar-light main_box">
				<div class="container">
					<!-- Brand and toggle get grouped for better mobile display -->
					<a class="navbar-brand logo_h"
						href="${pageContext.request.contextPath}/<c:url value="Resources/customer"/>/index.html"><img
						src="${pageContext.request.contextPath}/<c:url value="Resources/customer"/>/img/logo.png"
						alt=""></a>
					<button class="navbar-toggler" type="button" data-toggle="collapse"
						data-target="#navbarSupportedContent"
						aria-controls="navbarSupportedContent" aria-expanded="false"
						aria-label="Toggle navigation">
						<span class="icon-bar"></span> <span class="icon-bar"></span> <span
							class="icon-bar"></span>
					</button>
					<!-- Collect the nav links, forms, and other content for toggling -->
					<div class="collapse navbar-collapse offset"
						id="navbarSupportedContent">
						<ul class="nav navbar-nav menu_nav ml-auto">
							<li class="nav-item "><a class="nav-link"
								href="${pageContext.request.contextPath}//home">Home</a></li>
							<li class="nav-item "><a class="nav-link"
								href="${pageContext.request.contextPath}//category">Shop</a></li>
							<li class="nav-item "><a class="nav-link"
								href="${pageContext.request.contextPath}//blog">Blog</a></li>
							<li class="nav-item "><a class="nav-link"
								href="${pageContext.request.contextPath}//contact">Contact</a></li>
						</ul>
						<ul class="nav navbar-nav navbar-right">
							<li class="nav-item"><a class="nav-link"
								href="${pageContext.request.contextPath}//cart" class="cart">Cart</a></li>

							<c:if test="${empty user}">
								<li class="nav-item"><a class="nav-link"
									href="${pageContext.request.contextPath}/login" class="cart">Login</a></li>
							</c:if>
							<c:if test="${!empty user}">
								<li class="nav-item submenu dropdown"><a href="#"
									class="nav-link dropdown-toggle">${user.name}</a>
									<ul class="dropdown-menu">
										<li class="nav-item"><a class="nav-link">Order</a></li>
										<li class="nav-item"><a
											href="${pageContext.request.contextPath}/logout"
											class="nav-link">Log Out</a></li>
									</ul></li>
							</c:if>
						</ul>
					</div>
				</div>
			</nav>
		</div>

	</header>
	<!-- End Header Area -->

	<!-- Start Banner Area -->
	<section class="banner-area organic-breadcrumb">
		<div class="container">
			<div
				class="breadcrumb-banner d-flex flex-wrap align-items-center justify-content-end">
				<div class="col-first">
					<h1>Login/Register</h1>
					<nav class="d-flex align-items-center">
						<a
							href="${pageContext.request.contextPath}/<c:url value="Resources/customer"/>/index.html">Home<span
							class="lnr lnr-arrow-right"></span></a> <a
							href="${pageContext.request.contextPath}/<c:url value="Resources/customer"/>/category.html">Login/Register</a>
					</nav>
				</div>
			</div>
		</div>
	</section>
	<!-- End Banner Area -->

	<!--================Login Box Area =================-->
	<section class="login_box_area section_gap">
		<div class="container">
			<div class="row">
				<div class="col-lg-6">
					<div class="login_form_inner">
						<h3>Login</h3>
						<h3 style="color: red">${mess}</h3>
						<form:form class="row login_form" action="postLogin"
							novalidate="novalidate" modelAttribute="a" method="post">
							<div class="col-md-12 form-group">
								<label for="exampleInputEmail3">Email address</label> <input
									type="email" class="form-control" id="exampleInputEmail3"
									name="email" placeholder="Email">
							</div>
							<div class="col-md-12 form-group">
								<label for="exampleInputPassword4">Password</label> <input
									type="password" class="form-control" id="exampleInputPassword4"
									name="password" placeholder="Password">
							</div>
							<div class="col-md-12 form-group">
								<button type="submit" value="submit" class="primary-btn">Log
									In</button>
							</div>
						</form:form>
					</div>
				</div>
				<div class="col-lg-6">
					<div class="login_form_inner">
						<h3>Register</h3>
						<h3 style="color: red">${messs}</h3>
						<form:form class="row login_form" action="insertUser"
							novalidate="novalidate" modelAttribute="a" method="post"
							enctype="multipart/form-data">
							<div class="col-md-12 form-group">
								<label>User Name</label>
								<form:input type="text" class="form-control" path="name"
									name="name" placeholder="Name" onfocus="this.placeholder = ''"
									onblur="this.placeholder = 'Name'"></form:input>
							</div>
							<div class="col-md-12 form-group">
								<label for="exampleInputEmail3">Email address</label>
								<form:input type="email" class="form-control"
									id="exampleInputEmail3" path="email" placeholder="Email" />
							</div>
							<div class="col-md-12 form-group">
								<label for="exampleSelectGender">Gender</label>
								<form:select class="form-control" id="exampleSelectGender"
									path="gender">
									<option value="0">Male</option>
									<option value="1">Female</option>
								</form:select>
							</div>
							<div class="col-md-12 form-group">
								<label>Age</label>
								<form:input type="email" class="form-control" path="age"
									placeholder="Age" onfocus="this.placeholder = ''"
									onblur="this.placeholder = 'Age'"></form:input>
							</div>
							<div class="col-md-12 form-group">
								<label>Phone</label>
								<form:input type="number" class="form-control" id="name"
									path="phone" placeholder="Phone"
									onfocus="this.placeholder = ''"
									onblur="this.placeholder = 'Phone'"></form:input>
							</div>
							<div class="col-md-12 form-group">
								<label>Password</label>
								<form:input type="password" class="form-control" path="password"
									id="name" name="password" placeholder="Password"
									onfocus="this.placeholder = ''"
									onblur="this.placeholder = 'Password'"></form:input>

							</div>
							<div class="col-md-12 form-group">
								<label>Re-Password</label> <input type="password"
									class="form-control" id="name" name="re_password"
									placeholder="Re_Password" onfocus="this.placeholder = ''"
									onblur="this.placeholder = 'Re_Password'">
							</div>
							<div class="col-md-12 form-group">
								<label>Address</label>
								<form:textarea rows="5" cols="20" class="form-control"
									path="address" placeholder="Address"
									onfocus="this.placeholder = ''"
									onblur="this.placeholder = 'Address'"></form:textarea>
							</div>

							<div class="col-md-12 form-group">
								<form:button type="submit" class="primary-btn">Register
                        </form:button>
							</div>
						</form:form>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!--================End Login Box Area =================-->

	<!-- start footer Area -->
	<footer class="footer-area section_gap">
		<div class="container">
			<div class="row">
				<div class="col-lg-3  col-md-6 col-sm-6">
					<div class="single-footer-widget">
						<h6>About Us</h6>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit,
							sed do eiusmod tempor incididunt ut labore dolore magna aliqua.</p>
					</div>
				</div>
				<div class="col-lg-4  col-md-6 col-sm-6">
					<div class="single-footer-widget">
						<h6>Newsletter</h6>
						<p>Stay update with our latest</p>
						<div class="" id="mc_embed_signup">

							<form target="_blank" novalidate="true"
								action="https://spondonit.us12.list-manage.com/subscribe/post?u=1462626880ade1ac87bd9c93a&amp;id=92a4423d01"
								method="get" class="form-inline">

								<div class="d-flex flex-row">

									<input class="form-control" name="EMAIL"
										placeholder="Enter Email" onfocus="this.placeholder = ''"
										onblur="this.placeholder = 'Enter Email '" required=""
										type="email">


									<button class="click-btn btn btn-default">
										<i class="fa fa-long-arrow-right" aria-hidden="true"></i>
									</button>
									<div style="position: absolute; left: -5000px;">
										<input name="b_36c4fd991d266f23781ded980_aefe40901a"
											tabindex="-1" value="" type="text">
									</div>

									<!-- <div class="col-lg-4 col-md-4">
													<button class="bb-btn btn"><span class="lnr lnr-arrow-right"></span></button>
												</div>  -->
								</div>
								<div class="info"></div>
							</form>
						</div>
					</div>
				</div>
				<div class="col-lg-3  col-md-6 col-sm-6">
					<div class="single-footer-widget mail-chimp">
						<h6 class="mb-20">Instragram Feed</h6>
						<ul class="instafeed d-flex flex-wrap">
							<li><img
								src="${pageContext.request.contextPath}/<c:url value="Resources/customer"/>/img/i1.jpg"
								alt=""></li>
							<li><img
								src="${pageContext.request.contextPath}/<c:url value="Resources/customer"/>/img/i2.jpg"
								alt=""></li>
							<li><img
								src="${pageContext.request.contextPath}/<c:url value="Resources/customer"/>/img/i3.jpg"
								alt=""></li>
							<li><img
								src="${pageContext.request.contextPath}/<c:url value="Resources/customer"/>/img/i4.jpg"
								alt=""></li>
							<li><img
								src="${pageContext.request.contextPath}/<c:url value="Resources/customer"/>/img/i5.jpg"
								alt=""></li>
							<li><img
								src="${pageContext.request.contextPath}/<c:url value="Resources/customer"/>/img/i6.jpg"
								alt=""></li>
							<li><img
								src="${pageContext.request.contextPath}/<c:url value="Resources/customer"/>/img/i7.jpg"
								alt=""></li>
							<li><img
								src="${pageContext.request.contextPath}/<c:url value="Resources/customer"/>/img/i8.jpg"
								alt=""></li>
						</ul>
					</div>
				</div>
				<div class="col-lg-2 col-md-6 col-sm-6">
					<div class="single-footer-widget">
						<h6>Follow Us</h6>
						<p>Let us be social</p>
						<div class="footer-social d-flex align-items-center">
							<a
								href="${pageContext.request.contextPath}/<c:url value="Resources/customer"/>/#"><i
								class="fa fa-facebook"></i></a> <a
								href="${pageContext.request.contextPath}/<c:url value="Resources/customer"/>/#"><i
								class="fa fa-twitter"></i></a> <a
								href="${pageContext.request.contextPath}/<c:url value="Resources/customer"/>/#"><i
								class="fa fa-dribbble"></i></a> <a
								href="${pageContext.request.contextPath}/<c:url value="Resources/customer"/>/#"><i
								class="fa fa-behance"></i></a>
						</div>
					</div>
				</div>
			</div>
			<div
				class="footer-bottom d-flex justify-content-center align-items-center flex-wrap">
				<p class="footer-text m-0">
					<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
					Copyright &copy;
					<script>
						document.write(new Date().getFullYear());
					</script>
					All rights reserved | This template is made with <i
						class="fa fa-heart-o" aria-hidden="true"></i> by <a
						href="${pageContext.request.contextPath}/<c:url value="Resources/customer"/>/https://colorlib.com"
						target="_blank">Colorlib</a>
					<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
				</p>
			</div>
		</div>
	</footer>
	<!-- End footer Area -->


	<script
		src="${pageContext.request.contextPath}/<c:url value="Resources/customer"/>/js/vendor/jquery-2.2.4.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/<c:url value="Resources/customer"/>/https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"
		integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4"
		crossorigin="anonymous"></script>
	<script
		src="${pageContext.request.contextPath}/<c:url value="Resources/customer"/>/js/vendor/bootstrap.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/<c:url value="Resources/customer"/>/js/jquery.ajaxchimp.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/<c:url value="Resources/customer"/>/js/jquery.nice-select.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/<c:url value="Resources/customer"/>/js/jquery.sticky.js"></script>
	<script
		src="${pageContext.request.contextPath}/<c:url value="Resources/customer"/>/js/nouislider.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/<c:url value="Resources/customer"/>/js/jquery.magnific-popup.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/<c:url value="Resources/customer"/>/js/owl.carousel.min.js"></script>
	<!--gmaps Js-->
	<script
		src="${pageContext.request.contextPath}/<c:url value="Resources/customer"/>/https://maps.googleapis.com/maps/api/js?key=AIzaSyCjCGmQ0Uq4exrzdcL6rvxywDDOvfAu6eE"></script>
	<script
		src="${pageContext.request.contextPath}/<c:url value="Resources/customer"/>/js/gmaps.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/<c:url value="Resources/customer"/>/js/main.js"></script>

</body>
</html>