package project.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;


import project.entity.category;

import project.responsitory.category_respon;

@Controller
@RequestMapping("/api")
public class categoryController {

	@Autowired
	private category_respon category_respon;
	
	@ResponseBody
    @GetMapping("/getListCategory")
    public List<category> listCategory() {
    	return category_respon.findAll();
    }
    @ResponseBody
    @PostMapping("/insertCategory")
    public category insertCategory(@RequestBody category c ) {
    	return category_respon.save(c);
    }
    @ResponseBody
    @PutMapping("/updateCategory")
    public category updateCategory(@RequestBody category c) {
    	
        return category_respon.save(c);
	}
    @ResponseBody
    @DeleteMapping("/deleteCategoryById/{id}")
    public String deleteCategoryById(@PathVariable Integer id) {
    	category_respon.deleteById(id);
    	return "delete oke" ;
    }
    @ResponseBody
    @GetMapping("/getCategoryById/{id}")
    public category getCategoryById(@PathVariable Integer id) {
    	return category_respon.findByid(id);
    }
}
