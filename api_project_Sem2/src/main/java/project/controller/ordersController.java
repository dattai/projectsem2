package project.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;


import project.entity.orders;
import project.entity.product;
import project.responsitory.orders_respon;

@Controller
@RequestMapping("/api")
public class ordersController {
	@Autowired
	private orders_respon order_respon;
	
	@ResponseBody
    @GetMapping("/getListOrder")
    public List<orders> ListOrder() {
    	return order_respon.findAll();
    }
    @ResponseBody
    @PostMapping("/insertOrder")
    public orders insertOrder(@RequestBody orders o ) {
    	return order_respon.save(o);
    }

    
}
