package project.responsitory;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


import project.entity.product;

@Repository
public interface product_respon extends JpaRepository<product, Integer>{
	product findByid(Integer id);
}
 