package project.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.Gson;

import project.entity.users;
import project.responsitory.users_respon;

@Controller
@RequestMapping("/api")
public class usersController {
	Gson son= new Gson();
	@Autowired
	private users_respon users_respon;
	
	@ResponseBody
    @GetMapping("/getListUser")
    public List<users> getListUser() {
    	List<users> u = users_respon.findAll();
    	return u;
    }
    @ResponseBody
    @GetMapping("/getUserByEmail/{email}")
    public users getUserByEmail(@PathVariable String email) {
    	return users_respon.findByEmail(email);
    }
    @ResponseBody
    @PostMapping("/insertUser")
    public users insertUsers(@RequestBody users u ) {
    	return users_respon.save(u);
    }
    @ResponseBody
    @DeleteMapping("/deleteUserById/{id}")
    public List<users> deleteUsers(@PathVariable Integer id) {
    	users_respon.deleteById(id);
    	List<users> u = users_respon.findAll();
    	return  u;
    }
}
